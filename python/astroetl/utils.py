from copy import deepcopy
import numpy as np
import pyfits
import matplotlib.pyplot as plt
from astropy.table import Table
from sklearn.cluster import KMeans
from astroML.plotting.tools import draw_ellipse
from scipy.stats import gaussian_kde
from sklearn.mixture import GMM

import lsst.afw.table as afwTable
import lsst.afw.geom as afwGeom

def genCatFromTxt(fName, cNames, cNumbers, fTypes=None, cDocs=None, inDegrees=True, **kargs):
    """
    Load data from a `.txt` file into an LSST simple catalog.

    It's assumed that the column names in `cNames` contain the corresponding
    names for the fields of the LSST minimal schema, i.e. `id` `coord.ra` and `coord.dec`.
    """

    if cDocs is None:
        cDocs = ['']*len(cNames)

    if fTypes is None:
        fTypes = ['F']*len(cNames)

    data = np.genfromtxt(fName, names=cNames, usecols=cNumbers, **kargs)

    if inDegrees:
        data['coordra'] = np.radians(data['coordra'])
        data['coorddec'] = np.radians(data['coorddec'])

    schema = afwTable.SimpleTable.makeMinimalSchema()

    for i in range(len(cNames)):
        if cNames[i] not in ['id', 'coord.ra', 'coord.dec']:
            if fTypes[i] != 'String':
                schema.addField(afwTable.Field[fTypes[i]](cNames[i], cDocs[i]))
            else:
                schema.addField(afwTable.Field[fTypes[i]](cNames[i], cDocs[i], 10))

    cat = afwTable.SimpleCatalog(schema)
    n = len(data)
    cat.reserve(n)
    for i in range(n):
        cat.addNew()

    for i, name in enumerate(cNames):
        if fTypes[i] != 'String':
            cat[name][:] = data[name.replace(".", "")]
        else:
            for j, record in enumerate(cat):
                record[name] = data[name.replace(".", "")][j]
    
    return cat

def genCatFromFits(fName, cNames, fitsNames, fTypes=None, cDocs=None, inDegrees=True, twoDCoord=False):
    """
    Load data from a `.fits` file into an LSST simple catalog.

    It's assumed that the column names in `cNames` contain the corresponding
    names for the fields of the LSST minimal schema, i.e. `id` `coord.ra` and `coord.dec`.
    """

    if cDocs is None:
        cDocs = ['']*len(cNames)

    if fTypes is None:
        fTypes = ['F']*len(cNames)

    hdulist = pyfits.open(fName)
    tbdata = hdulist[1].data

    if inDegrees:
        if twoDCoord:
            tbdata[fitsNames[1]][:] = np.radians(tbdata[fitsNames[1]])
        else:
            tbdata[fitsNames[1]][:] = np.radians(tbdata[fitsNames[1]])
            tbdata[fitsNames[2]][:] = np.radians(tbdata[fitsNames[2]])


    schema = afwTable.SimpleTable.makeMinimalSchema()

    for i in range(len(cNames)):
        if cNames[i] not in ['id', 'coord.ra', 'coord.dec']:
            schema.addField(afwTable.Field[fTypes[i]](cNames[i], cDocs[i]))

    cat = afwTable.SimpleCatalog(schema)
    n = len(tbdata)
    cat.reserve(n)
    for i in range(n):
        cat.addNew()

    for i in range(len(cNames)):
        if not (twoDCoord and (cNames[i] in ['coord.ra', 'coord.dec'])):
            cat[cNames[i]][:] = tbdata[fitsNames[i]]
    if twoDCoord: 
        cat['coord.ra'][:] = tbdata[fitsNames[1]][:,0]
        cat['coord.dec'][:] = tbdata[fitsNames[1]][:,1]

    return cat

def genCatFromIpac(fName, cNames, ipacNames, fTypes=None, cDocs=None, inDegrees=True, twoDCoord=False):
    """
    Load data from a table in ipac format into an LSST simple catalog.

    It's assumed that the column names in `cNames` contain the corresponding
    names for the fields of the LSST minimal schema, i.e. `id` `coord.ra` and `coord.dec`.
    """
    
    if cDocs is None:
        cDocs = ['']*len(cNames)

    if fTypes is None:
        fTypes = ['F']*len(cNames)

    table = Table.read(fName, format='ipac')

    if inDegrees:
        if twoDCoord:
            table[ipacNames[1]].data.data[:] = np.radians(table[ipacNames[1]].data.data)
        else:
            table[ipacNames[1]].data.data[:] = np.radians(table[ipacNames[1]].data.data)
            table[ipacNames[2]].data.data[:] = np.radians(table[ipacNames[2]].data.data)

    schema = afwTable.SimpleTable.makeMinimalSchema()

    for i in range(len(cNames)):
        if cNames[i] not in ['id', 'coord.ra', 'coord.dec']:
            schema.addField(afwTable.Field[fTypes[i]](cNames[i], cDocs[i]))

    cat = afwTable.SimpleCatalog(schema)
    n = len(table)
    cat.reserve(n)
    for i in range(n):
        cat.addNew()

    for i in range(len(cNames)):
        if not (twoDCoord and (cNames[i] in ['coord.ra', 'coord.dec'])):
            try:
                cat[cNames[i]][:] = table[ipacNames[i]].data.data
            except ValueError:
                import ipdb; ipdb.set_trace()
    if twoDCoord: 
        cat['coord.ra'][:] = tbdata[ipacNames[1]].data.data[:,0]
        cat['coord.dec'][:] = tbdata[ipacNames[1]].data.data[:,1]

    return cat

def matchCats(cat1, cat2, matchRadius=1*afwGeom.arcseconds, includeMismatches=True, multiMeas=False):
    """
    Match to catalogs and return a catalog with the fields of the two catalogs
    """

    mc = afwTable.MatchControl()
    mc.includeMismatches = includeMismatches
    mc.findOnlyClosest = True

    matched = afwTable.matchRaDec(cat1, cat2, matchRadius, mc)

    bestMatches = {}
    if includeMismatches:
        noMatch = []
    for m1, m2, d in matched:
        if m2 is None:
            noMatch.append(m1)
        else:
            if not multiMeas:
                id = m2.getId()
                if id not in bestMatches:
                    bestMatches[id] = (m1, m2, d)
                else:
                    if d < bestMatches[id][2]:
                        bestMatches[id] = (m1, m2, d)
            else:
                id = m1.getId()
                bestMatches[id] = (m1, m2, d)

    if includeMismatches:
        print "{0} objects from {1} in the first catalog had no match in the second catalog.".format(len(noMatch), len(cat1))
        print "{0} objects from the first catalog with a match in the second catalog were not the closest match.".format(len(matched) - len(noMatch) - len(bestMatches))

    nMatches = len(bestMatches)
    print "I found {0} matches".format(nMatches)

    schema1 = cat1.getSchema(); schema2 = cat2.getSchema()
    names1 = cat1.schema.getNames(); names2 = cat2.schema.getNames()

    schema = afwTable.SimpleTable.makeMinimalSchema()

    catKeys = []; cat1Keys = []; cat2Keys = []
    for name in names1:
        cat1Keys.append(schema1.find(name).getKey())
        if name not in ['id', 'coord']:
            catKeys.append(schema.addField(schema1.find(name).getField()))
        else:
            catKeys.append(schema.find(name).getKey())
    for name in names2:
        cat2Keys.append(schema2.find(name).getKey())
        if name not in schema1.getNames():
            catKeys.append(schema.addField(schema2.find(name).getField()))
        elif name+".2" not in schema1.getNames():
            catKeys.append(schema.addField(schema2.find(name).getField().copyRenamed(name+".2")))
        else:
            catKeys.append(schema.addField(schema2.find(name).getField().copyRenamed(name+".3")))

    cat = afwTable.SimpleCatalog(schema)
    cat.reserve(nMatches)

    for id in bestMatches:
        m1, m2, d = bestMatches[id]
        record = cat.addNew()
        for i in range(len(cat1Keys)):
            record.set(catKeys[i], m1.get(cat1Keys[i]))
        for i in range(len(cat1Keys), len(catKeys)):
            record.set(catKeys[i], m2.get(cat2Keys[i-len(cat1Keys)]))

    return cat

def _getLayout(n):
    if n == 1:
        return 1, 1
    elif n == 2:
        return 1, 2
    elif n == 2:
        return 1, 3
    elif n == 4:
        return 2, 2
    else:
        raise ValueError('{0} cuts not implemented'.format(n))

def plotCutsHists(cat, dataField, cutField, cuts=None, logScale=False, normed=False, nBins=100
                  , muClass=None, xlim=None, ylim=None, xlabel=None, ylabel=None, cutName=None):
    """
    Plot histograms of `dataField` of the cuts `cuts` in the field `cutField`.
    """
    data = cat.get(dataField)
    cutData = cat.get(cutField)
    if muClass is not None:
        good = cat.get('mu.class') == muClass
        data = data[good]; cutData = cutData[good]

    if cuts is None:
        nCuts = 1
    else:
        nCuts = len(cuts)
    nRow, nColumn = _getLayout(nCuts)

    if xlim is not None:
        hist, nBins = np.histogram(data, bins=nBins, range=xlim)
    fig = plt.figure()
    for i in range(nRow*nColumn):
        if cuts is None:
            good = np.ones(data.shape, dtype=bool)
        else:
            good = np.logical_and(cutData > cuts[i][0], cutData < cuts[i][1])
        ax = fig.add_subplot(nRow, nColumn, i+1)
        if logScale:
            ax.hist(np.log10(data[good]), bins=nBins, normed=normed, histtype='step')
        else:
            ax.hist(data[good], bins=nBins, normed=normed, histtype='step')
        if xlim is not None:
            ax.set_xlim(xlim)
        if ylim is not None:
            ax.set_ylim(ylim)
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
        if cutName is not None:
            ax.set_title("{0} < {1} < {2}".format(cuts[i][0], cutName, cuts[i][1]), fontsize=12)
    return fig

def loadAcsCat(fName='/u/garmilla/Data/Alexie/astrometry.acs_iphot_sep0719445.tbl'):
    cNames = ['id', 'coord.ra', 'coord.dec', 'mag_auto', 'mu_max', 'mu_class']
    fTypes = ['I', 'F', 'F', 'F', 'F', 'I']
    ipacNames = ['xnumber', 'ra', 'dec', 'mag_auto', 'mu_max', 'mu_class']
    cat = genCatFromIpac(fName, cNames, ipacNames, fTypes=fTypes)
    return cat

def loadSpecCat(fName='/u/garmilla/Data/Alexie/OBSERVED_TARGETS_15April2015_withHeader.dat'):
    cNames = ['Instr', 'coord.ra', 'coord.dec', 'ORI_ID', 'z_spec', 'Q_f', 'OPT_ID', 'ra_opt', 'dec_opt', 'group_id'] 
    cNumbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10]
    fTypes = ['String', 'F', 'F', 'String', 'F', 'F', 'F', 'F', 'F', 'I']
    dtype = "S10,f8,f8,S10,f8,f8,f8,f8,f8,i8"
    cat = genCatFromTxt(fName, cNames, cNumbers, fTypes=fTypes, dtype=dtype)
    return cat

def genSDSSPhotoSpecTrainingFromFits(fName):
    hdulist = pyfits.open(fName)
    tbdata = hdulist[1].data
    
    X = np.zeros((len(tbdata), 5))
    Xerr= np.zeros((len(tbdata), 5))
    for i, band in enumerate(['u', 'g', 'r', 'i', 'z']):
        X[:, i] = tbdata["cModelMag_"+band] - tbdata["extinction_"+band]
        Xerr[:, i] = tbdata["cModelMagErr_"+band]
        #X[:, 5 + i] = tbdata["psfMag_"+band] - tbdata["cModelMag_"+band]
    Ychar = tbdata['class']
    Y = np.zeros(Ychar.shape, dtype=int)
    gals = np.logical_and(True, Ychar == 'GALAXY'); Y[gals] = 0
    qsos = np.logical_and(True, Ychar == 'QSO'); Y[qsos] = 1
    stars = np.logical_and(True, Ychar == 'STAR'); Y[stars] = 2
    Xheader = "cModelMag_u, cModelMag_g, cModelMag_r, cModelMag_i, cModelMag_z"
    XerrHeader = "cModelMagErr_u, cModelMagErr_g, cModelMagErr_r, cModelMagErr_i, cModelMagErr_z"
    Yheader = "Spectroscopic Class {0:Galaxy, 1:Quasar, 2:Star}"
    np.savetxt('magnitudes.csv', X, delimiter=',', header=Xheader)
    np.savetxt('magnitudeErrors.csv', Xerr, delimiter=',', header=XerrHeader)
    np.savetxt('class.csv', Y, delimiter=',', fmt='%i', header=Yheader)

def genAcsPhotozMatch(acsFile, photozFile):
    acsCNames = ['id', 'coord.ra', 'coord.dec', 'mag_auto', 'mu_max', 'mu_class']
    ipacNames = ['xnumber', 'ra', 'dec', 'mag_auto', 'mu_max', 'mu_class']
    acsFTypes = ['I', 'F', 'F', 'F', 'F', 'I']
    acsCat = genCatFromIpac(acsFile, acsCNames, ipacNames, fTypes=acsFTypes)
    photozCNames = ['id', 'coord.ra', 'coord.dec', 'chi2_2', 'chiq', 'chis']
    cNumbers = [0, 1, 2, 16, 19, 22]
    photozFTypes = ['I', 'F', 'F', 'F', 'F', 'F']
    photozCat = genCatFromTxt(photozFile, photozCNames, cNumbers, fTypes=photozFTypes)
    match = matchCats(acsCat, photozCat)
    return match

_vistaFilters = ['u', 'B', 'V', 'r', 'ip', 'zpp', 'IB427', 'IB464', 'IA484', 'IB505', 'IA527',
                 'IB574', 'IA624', 'IA679', 'IA738', 'IA767', 'IB709', 'IB827', 'NB711', 'NB816',
                 'yHSC', 'Y', 'J', 'H', 'Ks', 'Hw', 'Ksw', 'SPLASH_1', 'SPLASH_2', 'SPLASH_3',
                 'SPLASH_4', '24']

def genAcsVistaMatch(acsFile, vistaFile, magType='AUTO'):
    acsCNames = ['id', 'coord.ra', 'coord.dec', 'mag_auto', 'mu_max', 'mu_class']
    ipacNames = ['xnumber', 'ra', 'dec', 'mag_auto', 'mu_max', 'mu_class']
    acsFTypes = ['I', 'F', 'F', 'F', 'F', 'I']
    acsCat = genCatFromIpac(acsFile, acsCNames, ipacNames, fTypes=acsFTypes)
    fitsNames = ['NUMBER', 'ALPHA_J2000', 'DELTA_J2000', 'FLAG_HJMCC', 'FLAG_DEEP', 'FLAG_PETER',
                  'EBV']
    for filter in _vistaFilters:
        if not ('SPLASH' in filter or '24' == filter):
            fitsNames.append(filter + '_MAG_' + magType)
            fitsNames.append(filter + '_MAGERR_' + magType)
        else:
            fitsNames.append(filter + '_MAG')
            fitsNames.append(filter + '_MAGERR')
    vistaCNames = deepcopy(fitsNames)
    vistaCNames[0] = 'id'; vistaCNames[1] = 'coord.ra'; vistaCNames[2] = 'coord.dec'
    vistaFTypes = ['F']*len(vistaCNames)
    for i in [0, 3, 4, 5, 6]:
        vistaFTypes[i] = 'I'
    vistaCat = genCatFromFits(vistaFile, vistaCNames, fitsNames, fTypes=vistaFTypes)
    match = matchCats(acsCat, vistaCat)
    return match

def genAcsVistaSpecMatch(acsFile, vistaFile, specFile):
    acsVistaCat = genAcsVistaMatch(acsFile, vistaFile)
    cNames = ['Instr', 'coord.ra', 'coord.dec', 'ORI_ID', 'z_spec', 'Q_f', 'OPT_ID', 'ra_opt', 'dec_opt', 'group_id'] 
    cNumbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10]
    fTypes = ['String', 'F', 'F', 'String', 'F', 'F', 'F', 'F', 'F', 'I']
    dtype = "S10,f8,f8,S10,f8,f8,f8,f8,f8,i8"
    specCat = genCatFromTxt(specFile, cNames, cNumbers, fTypes=fTypes, dtype=dtype)
    match = matchCats(acsVistaCat, specCat)
    return match

def genAcsSpecMatch(acsFile='/u/garmilla/Data/Alexie/astrometry.acs_iphot_sep0719445.tbl',
                    specFile='/u/garmilla/Data/Alexie/OBSERVED_TARGETS_15April2015_withHeader.dat'):
    acsCat = loadAcsCat(fName=acsFile)
    specCat = loadSpecCat(fName=specFile)
    match = matchCats(acsCat, specCat)
    return match

def genHscSpecMatch(hscFile='/u/garmilla/Data/HSC/pMatchDeepCoaddHsc-119320150325GRIZY.fits',
                    specFile='/u/garmilla/Data/Alexie/OBSERVED_TARGETS_15April2015_withHeader.dat'):
    hscCat = afwTable.SimpleCatalog.readFits(hscFile)
    specCat = loadSpecCat(fName=specFile)
    match = matchCats(hscCat, specCat)
    return match

def genHscAcsSpecMatch(hscFile='/u/garmilla/Data/HSC/pMatchDeepCoaddHsc-119320150325GRIZY.fits',
                       acsFile='/u/garmilla/Data/Alexie/astrometry.acs_iphot_sep0719445.tbl',
                       specFile='/u/garmilla/Data/Alexie/OBSERVED_TARGETS_15April2015_withHeader.dat'):
    acsCat = loadAcsCat(fName=acsFile)
    hscCat = afwTable.SimpleCatalog.readFits(hscFile)
    specCat = loadSpecCat(fName=specFile)
    match = matchCats(acsCat, hscCat)
    match = matchCats(match, specCat)
    return match

def getHscSpecOdd(match=None):
    if match is None:
        match = genHscSpecMatch()
    good = np.logical_and(match.get('z_spec') == 0.0, np.logical_and(match.get('Q_f') >= 3.0, match.get('Q_f') <= 6.0))
    magG = -2.5*np.log10(match.get('cmodel.flux.g')/match.get('flux.zeromag.g'))
    magR = -2.5*np.log10(match.get('cmodel.flux.r')/match.get('flux.zeromag.r'))
    magI = -2.5*np.log10(match.get('cmodel.flux.i')/match.get('flux.zeromag.i'))
    odd = np.logical_and(magG[good] - magR[good] < 1.17, magR[good]-magI[good] > 0.07 + (0.6-0.07)*(magG[good]-magR[good]))
    idSpecOdd = match.get('id.2')[good][odd]
    groupSpecOdd = match.get('group_id')[good][odd]
    return idSpecOdd, groupSpecOdd

def getAcsSpecOdd():
    match = genAcsSpecMatch()
    magAuto = match.get('mag_auto')
    muMax = match.get('mu_max')
    good = np.logical_and(match.get('z_spec') == 0.0, np.logical_and(match.get('Q_f') >= 3.0, match.get('Q_f') <= 6.0))
    odd = np.logical_and(np.logical_and(good, muMax > magAuto - 3.8), muMax > -magAuto + 2*16.5)
    idSpecOdd = match.get('id.2')[odd]
    groupSpecOdd = match.get('group_id')[odd]
    return idSpecOdd, groupSpecOdd

def getHscAcsOdd(match=None, idSpecOdd=None, groupSpecOdd=None, hscMatch=None):
    if idSpecOdd is None or groupSpecOdd is None:
        idSpecOdd, groupSpecOdd = getHscSpecOdd(match=hscMatch)
    if match is None:
        match = genAcsSpecMatch()
    idSpec = match.get('id.2')
    groupSpec = match.get('group_id')
    odd = np.zeros(idSpec.shape, dtype=bool)
    for i, id in enumerate(idSpec):
        if id in idSpecOdd:
            odd[i] = True
        if groupSpec[i] != 0 and groupSpec[i] in groupSpecOdd:
            odd[i] = True
    return odd

def getAcsHscOdd(match=None, idSpecOdd=None, groupSpecOdd=None):
    if idSpecOdd is None or groupSpecOdd is None:
        idSpecOdd, groupSpecOdd = getAcsSpecOdd()
    if match is None:
        match = genHscSpecMatch()
    idSpec = match.get('id.2')
    groupSpec = match.get('group_id')
    odd = np.zeros(idSpec.shape, dtype=bool)
    for i, id in enumerate(idSpec):
        if id in idSpecOdd:
            odd[i] = True
        if groupSpec[i] != 0 and groupSpec[i] in groupSpecOdd:
            odd[i] = True
    return odd

def plotHscAcsOdd(hscCat, acsCat, goodHsc=None, goodAcs=None, oddAcs=None, oddHsc=None):
    magG = -2.5*np.log10(hscCat.get('cmodel.flux.g')/hscCat.get('flux.zeromag.g'))
    magR = -2.5*np.log10(hscCat.get('cmodel.flux.r')/hscCat.get('flux.zeromag.r'))
    magI = -2.5*np.log10(hscCat.get('cmodel.flux.i')/hscCat.get('flux.zeromag.i'))
    if goodHsc is None or oddHsc is None:
        goodHsc = np.logical_and(hscCat.get('z_spec') == 0.0, np.logical_and(hscCat.get('Q_f') >= 3.0, hscCat.get('Q_f') <= 6.0))
        oddHsc = np.logical_and(goodHsc, np.logical_and(magG - magR < 1.17, magR-magI > 0.07 + (0.6-0.07)*(magG-magR)))
    if oddAcs is None:
        oddAcs = getHscAcsOdd(match=acsCat, hscMatch=hscCat)
    if goodAcs is None:
        goodAcs = np.logical_and(acsCat.get('z_spec') == 0.0, np.logical_and(acsCat.get('Q_f') >= 3.0, acsCat.get('Q_f') <= 6.0))

    fig = plt.figure()
    magAuto = acsCat.get('mag_auto')
    muMax = acsCat.get('mu_max')
    axAcs = fig.add_subplot(1, 2, 1)
    axAcs.set_xlabel('MAG_AUTO')
    axAcs.set_ylabel('MU_MAX')
    axAcs.scatter(magAuto[goodAcs], muMax[goodAcs], marker='.', s=2, color='black')
    axAcs.scatter(magAuto[oddAcs], muMax[oddAcs], marker='.', s=3, color='red')

    axHsc = fig.add_subplot(1, 2, 2)
    axHsc.set_xlabel('g-r')
    axHsc.set_ylabel('r-i')
    axHsc.scatter(magG[goodHsc]-magR[goodHsc], magR[goodHsc]-magI[goodHsc], marker='.', s=2)
    axHsc.scatter(magG[oddHsc]-magR[oddHsc], magR[oddHsc]-magI[oddHsc], marker='.', s=3, color='red')

    return fig

def plotAcsHscOdd(acsCat, hscCat, goodAcs=None, goodHsc=None, oddHsc=None, oddAcs=None):
    if goodHsc is None:
        goodHsc = np.logical_and(hscCat.get('z_spec') == 0.0, np.logical_and(hscCat.get('Q_f') >= 3.0, hscCat.get('Q_f') <= 6.0))
    if oddHsc is None:
        oddHsc = getHscAcsOdd()
    if oddAcs is None or goodAcs is None:
        magAuto = acsCat.get('mag_auto')
        muMax = acsCat.get('mu_max')
        goodAcs = np.logical_and(acsCat.get('z_spec') == 0.0, np.logical_and(acsCat.get('Q_f') >= 3.0, acsCat.get('Q_f') <= 6.0))
        oddAcs = np.logical_and(np.logical_and(goodAcs, muMax > magAuto - 3.8), muMax > -magAuto + 2*16.5)

    fig = plt.figure()
    magAuto = acsCat.get('mag_auto')
    muMax = acsCat.get('mu_max')
    axAcs = fig.add_subplot(1, 2, 1)
    axAcs.set_xlabel('MAG_AUTO')
    axAcs.set_ylabel('MU_MAX')
    axAcs.scatter(magAuto[goodAcs], muMax[goodAcs], marker='.', s=2, color='black')
    axAcs.scatter(magAuto[oddAcs], muMax[oddAcs], marker='.', s=3, color='red')

    magG = -2.5*np.log10(hscCat.get('cmodel.flux.g')/hscCat.get('flux.zeromag.g'))
    magR = -2.5*np.log10(hscCat.get('cmodel.flux.r')/hscCat.get('flux.zeromag.r'))
    magI = -2.5*np.log10(hscCat.get('cmodel.flux.i')/hscCat.get('flux.zeromag.i'))
    axHsc = fig.add_subplot(1, 2, 2)
    axHsc.set_xlabel('g-r')
    axHsc.set_ylabel('r-i')
    axHsc.scatter(magG[goodHsc]-magR[goodHsc], magR[goodHsc]-magI[goodHsc], marker='.', s=2)
    axHsc.scatter(magG[oddHsc]-magR[oddHsc], magR[oddHsc]-magI[oddHsc], marker='.', s=3, color='red')

    return fig

def getHscAcsSpecMismatches(catHsc=None, catAcs=None, colHsc=None, colAcs=None):
    if catHsc is None:
        catHsc = genHscSpecMatch()
    if catAcs is None:
        catAcs = genAcsSpecMatch()
    specIdHsc = catHsc.get('id.2')
    inHscNotAcs = np.zeros(specIdHsc.shape, dtype=bool)
    gIdHsc = catHsc.get('group_id')
    gIdAcs = catAcs.get('group_id')
    specIdAcs = catAcs.get('id.2')
    inAcsNotHsc = np.zeros(specIdAcs.shape, dtype=bool)

    for i, id in enumerate(specIdHsc):
        gId = gIdHsc[i]
        if gId == 0:
            if id not in specIdAcs:
                inHscNotAcs[i] = True
        else:
            if id not in specIdAcs and gId not in gIdAcs:
                inHscNotAcs[i] = True

    for i, id in enumerate(specIdAcs):
        gId = gIdAcs[i]
        if gId == 0:
            if id not in specIdHsc:
                inAcsNotHsc[i] = True
        else:
            if id not in specIdHsc and gId not in gIdHsc:
                inAcsNotHsc[i] = True

    if colHsc is not None and colAcs is not None:
        retHsc = catHsc.get(colHsc)
        retAcs = catAcs.get(colAcs)

        return retHsc[inHscNotAcs], retAcs[inAcsNotHsc]
    else:
        return inHscNotAcs, inAcsNotHsc

def _plotAcsDensityScipy(cat=None, nPoints=100, scale=-3.5):
    if cat is None:
        cat = loadAcsCat()
    magAuto = cat.get('mag_auto')
    muMax = cat.get('mu_max')
    good = np.logical_and(True, magAuto > 0.0)
    data = np.vstack((magAuto[good], muMax[good]))
    kde = gaussian_kde(data)
    x = np.linspace(17.0, 30.0, num=nPoints)
    y = np.linspace(13.0, 25.0, num=nPoints)
    X, Y = np.meshgrid(x, y)
    points = np.vstack((X.flatten(), Y.flatten()))
    Z = kde(points); Z = Z.reshape((nPoints, nPoints))
    fig = plt.figure()
    plt.contour(X, Y, np.log10(np.power(10.0, scale)+Z))
    starx=[9   ,17  ,19,25.5    ,26.5  ,9.2]
    stary=[14.5,14.5,15  ,21.5  ,21.5,4.2]
    for i in range(len(starx)-1):
        plt.plot([starx[i], starx[i+1]], [stary[i], stary[i+1]], color='k', linewidth=2)
    plt.xlabel('MAG_AUTO')
    plt.ylabel('MU_MAX')
    plt.xlim((x.min(), x.max()))
    plt.ylim((y.min(), y.max()))
    return fig

def _plotAcsDensityGmm(cat=None, nPoints=100, scale=-8.0):
    if cat is None:
        cat = loadAcsCat()
    Xtrain = genTrainingSet(cat, ['mu_max', 'mag_auto'])
    gmm = GMM(n_components=20, covariance_type='full', random_state=2)
    gmm.fit(Xtrain)
    x = np.linspace(17.0, 30.0, num=nPoints)
    y = np.linspace(13.0, 25.0, num=nPoints)
    X, Y = np.meshgrid(x, y)
    points = np.vstack((Y.flatten(), X.flatten()))
    Z = np.power(10.0, gmm.score(points.T)); Z = Z.reshape((nPoints, nPoints))
    fig = plt.figure()
    plt.contour(X, Y, np.log10(np.power(10.0, scale)+Z))
    starx=[9   ,17  ,19,25.5    ,26.5  ,9.2]
    stary=[14.5,14.5,15  ,21.5  ,21.5,4.2]
    for i in range(len(starx)-1):
        plt.plot([starx[i], starx[i+1]], [stary[i], stary[i+1]], color='k', linewidth=2)
    plt.xlabel('MAG_AUTO')
    plt.ylabel('MU_MAX')
    plt.xlim((x.min(), x.max()))
    plt.ylim((y.min(), y.max()))
    return fig
    #return plotGmmFit(Xtrain, gmm)

def plotAcsDensity(method='scipy', **kargs):
    if method == 'scipy':
        return _plotAcsDensityScipy(**kargs)
    elif method == 'gmm':
        return _plotAcsDensityGmm(**kargs)
    else:
        raise ValueError("Method {0} is not implemented".format(method))

def genAlexieTxtDat(match):
    good = np.logical_and(match.get('z_spec') == 0.0, np.logical_and(match.get('Q_f') >= 3.0, match.get('Q_f') <= 6.0))
    good = np.logical_and(good, match.get('mag_auto') > 22.0)
    ra = np.degrees(match.get('coord.ra')[good])
    dec = np.degrees(match.get('coord.dec')[good])
    group_id = match.get('group_id')[good]
    Instr = []; ORI_ID = []
    for i, record in enumerate(match):
        if good[i]:
            Instr.append(record.get('Instr'))
            ORI_ID.append(record.get('ORI_ID'))
    assert len(Instr) == len(ra)
    with open('haloStars.txt', 'w') as f:
        f.write('Instr, ORI_ID, ra, dec, group_id\n')
        for i in range(np.sum(good)):
            f.write("{0}, {1}, {2}, {3}, {4}\n".format(Instr[i], ORI_ID[i], ra[i], dec[i], group_id[i]))
    
def genAlexiePlot(match, magRange=(24.8, 26.0), cutWidth=0.2, starx=[9.0, 17.0, 19.0, 25.5, 26.5, 9.2],
                  stary=[14.5, 14.5, 15.0, 21.5, 21.5, 4.2], mode='diff', xlim=(-5.0, 5.0), logData=False):
    
    nCuts = int((magRange[1] - magRange[0])/cutWidth) + 1
    
    magAuto = match.get('mag_auto')
    muMax = match.get('mu_max')
    muClass = match.get('mu_class')
    chi2_2 = match.get('chi2_2')
    chis = match.get('chis')

    if mode == 'diff':
        data = chi2_2 - chis
        xlabel = r'$\chi_{gal}^2 - \chi_{star}^2$'
    elif mode == 'star':
        data = chis
        xlabel = r'$\chi_{star}^2$'
    elif mode == 'gal':
        data = chi2_2
        xlabel = r'$\chi_{gal}^2$'
    else:
        raise ValueError('Mode {0} not implemented'.format(mode))

    if logData:
        data = np.log10(data)
    fig = plt.figure()

    for i in range(nCuts):
        ax = fig.add_subplot(2, 3, i+1)
        ax.set_xlabel(xlabel)
        ax.set_ylabel('mu_max')
        magCut = (magRange[0] + cutWidth*i, magRange[0] + cutWidth*(i+1))
        good = np.logical_and(magAuto > magCut[0], magAuto < magCut[1])
        star = np.logical_and(good, muClass == 2)
        galaxy = np.logical_and(good, muClass == 1)
        #ax.scatter(chi2_2[good]-chis[good], muMax[good], marker='.', s=1)
        ax.scatter(data[galaxy], muMax[galaxy], marker='.', s=1, color='red')
        ax.scatter(data[star], muMax[star], marker='.', s=1, color='blue')
        ax.set_title("{0} < mag_auto < {1}".format(magCut[0], magCut[1]))
        ax.set_xlim(xlim)
        ax.set_ylim((20.0, 24.5))

    return fig

def genTrainingSet(cat, xFields, yField=None, yDtype=int, onlyFinite=True, lowCut=None):
    """
    Generate a training set for scikit algorithms.
    """
    X = np.zeros((len(cat), len(xFields)))
    if yField is not None:
        Y = np.zeros((len(cat),), dtype=yDtype)

    for i, field in enumerate(xFields):
        X[:,i] = cat.get(field)
    if yField is not None:
        Y[:] = cat.get(yField)

    good = True
    if onlyFinite:
        for i in range(len(xFields)):
            good = np.logical_and(good, np.isfinite(X[:,i]))
        if yField is not None:
            good = np.logical_and(good, np.isfinite(Y))

    if lowCut is not None:
        good = np.logical_and(good, cat.get(lowCut[0]) > lowCut[1])

    if len(good) == len(X):
        X = X[good]
        if yField is not None:
            Y = Y[good]

    if yField is None:
        return X
    else:
        return X, Y

def makeColors(X, colorIdxs):

    nColors = len(colorIdxs)
    Xcolors = np.zeros((X.shape[0], X.shape[1]-nColors))
    colorIdxSet = set()

    for pair in colorIdxs:
        colorIdxSet.add(pair[0])
        colorIdxSet.add(pair[1])

    idxs = set(range(X.shape[1]))
    noColorSet = idxs.difference(colorIdxSet)

    for i, idx in enumerate(noColorSet):
        Xcolors[:,i] = X[:,idx]
        print "Mapping index {0} to index {1}".format(idx, i)
    offset = i+1

    for i, pair in enumerate(colorIdxs):
        Xcolors[:, offset+i] = X[:, pair[0]] - X[:, pair[1]]
        print "Mapping indexes {0} to index {1}".format(pair, offset+i)

    return Xcolors

def checkLimits(X, limits):
    good = True
    for i in limits:
        good = np.logical_and(good, X[:, i] > limits[i][0])
        good = np.logical_and(good, X[:, i] < limits[i][1])
    return good

def getLabels(gmm, idxMagAuto=1, idxMuMax=0):
    means = gmm.means_
    labels = np.zeros((means.shape[0],))
    for i in range(len(means)):
        labels[i] =  means[i][idxMuMax] < 15.0 or means[i][idxMuMax] < means[i][idxMagAuto] - 4.
    return labels

def plotGmmFit(X, gmm, xIndex=1, yIndex=0, multi=False, xlabel='MAG_AUTO', ylabel='MU_MAX',
               xlim=(12.0, 30.0), ylim=(13.0, 25.0), A=None, labels=None):
    """
    Plot the mixture model `gmm` fit to the data `X`.
    kargs:
        xIndex: Index of the quantity in the x axis.
        yIndex: Index of the quantity in the y axis.
        mode: If multi plot scatter plot and ellipses, if ellipses
              only plot ellipses.
        A: If not None, it's a matrix that can be used to plot linear combinations
           of the features in X.
        labels: If not None, then a boolean vector where True means the ellipse
               is associated with stars.
    """
    
    means = gmm.means_
    covars = gmm._get_covars()

    if A is not None:
        meansNew = np.zeros(means.shape)
        covarsNew = np.zeros(covars.shape)
        for i in range(len(means)):
            meansNew[i] = np.dot(A, means[i])
            covarsNew[i] = np.dot(np.dot(A, covars[i]), A.T)
        Xnew = np.zeros(X.shape)
        for i in range(len(X)):
            Xnew[i] = np.dot(A, X[i])
        means = meansNew
        covars = covarsNew
        X = Xnew

    subArray = ([xIndex, yIndex])
    subMatrix = np.ix_([xIndex, yIndex], [xIndex, yIndex])
    fig = plt.figure()
    if multi: 
        ax1 = fig.add_subplot(1, 2, 1)
        ax1.scatter(X[:, xIndex], X[:, yIndex], marker='.', s=1, color='black')
        ax1.set_xlabel(xlabel)
        ax1.set_ylabel(ylabel)
        ax1.set_xlim(xlim)
        ax1.set_ylim(ylim)
        ax2 = fig.add_subplot(1, 2, 2)
        if labels is not None:
            for i, label in enumerate(labels):
                if label:
                    draw_ellipse(means[i][subArray], covars[i][subMatrix], scales=[2], ax=ax2, ec='k', fc='blue', alpha=max(gmm.weights_[i], 0.05))
                else:
                    draw_ellipse(means[i][subArray], covars[i][subMatrix], scales=[2], ax=ax2, ec='k', fc='red', alpha=max(gmm.weights_[i], 0.05))
        else:
            for i in range(len(means)):
                draw_ellipse(means[i][subArray], covars[i][subMatrix], scales=[2], ax=ax2, ec='k', fc='gray', alpha=max(gmm.weights_[i], 0.05))
        ax2.set_xlabel(xlabel)
        ax2.set_ylabel(ylabel)
        ax2.set_xlim(xlim)
        ax2.set_ylim(ylim)
    else:
        ax = fig.add_subplot(1, 1, 1)
        if labels is not None:
            for i, label in enumerate(labels):
                if label:
                    draw_ellipse(means[i][subArray], covars[i][subMatrix], scales=[2], ax=ax, ec='k', fc='blue', alpha=max(gmm.weights_[i], 0.05))
                else:
                    draw_ellipse(means[i][subArray], covars[i][subMatrix], scales=[2], ax=ax, ec='k', fc='red', alpha=max(gmm.weights_[i], 0.05))
        else:
            for i in range(len(means)):
                draw_ellipse(means[i][subArray], covars[i][subMatrix], scales=[2], ax=ax, ec='k', fc='gray', alpha=max(gmm.weights_[i], 0.05))
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

    return fig
